window.api_uri = 'https://smsonline.fr/Api';
window.sms_uri = 'https://smsonline.fr/fr/messages';

escapeHTML.replacements = { "&": "&amp;", '"': "&quot;", "<": "&lt;", ">": "&gt;" };

function notifyClient (text) {
    var opt = {
        'icon': '/img/logo-128.png'
    };
    console.log("Notif called at " + Date().toString());
    if (!("Notification" in window)) {
        return;
    } else if (Notification.permission === "granted") {

        var notification = new Notification(text, opt);
    } else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            if(!('permission' in Notification)) {
                Notification.permission = permission;
            }
            if (permission === "granted") {
                var notification = new Notification(text, opt);
            }
        });
    }
}

function escapeHTML(str) {
    if (str != "" && str != undefined && !Number.isInteger(str)) {
        str.replace(/[&"<>]/g, function (m) { escapeHTML.replacements[m];});
    }
    return str;
}

function locale(match, v1) {
    return (v1 && chrome.i18n.getMessage(v1) != "") ? chrome.i18n.getMessage(v1) : match;
}
