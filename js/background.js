DEFAULT_ENCODE = function(a) { return a; };
DEFAULT_DECODE = function(a) { return a; };

window.unread = null;

function getInfo() {
    var opt = {
        'url': window.api_uri + '/Users/GetUnread',
        'data': {
            'user': Storage.get('user', ''),
            'token': Storage.get('token', ''),
            'key': Storage.get('key', '')
        },
        'callback': getInfoRes,
        'checkErrors': false,
        'decode': false
    };
    Ajax.get(opt);
}

function getInfoRes(jsonData) {
    try {
        var data = JSON.parse(jsonData);
        if (data.error == 0) {
            if (data.messages_unread > 0) {
                chrome.browserAction.setBadgeText({text: data.messages_unread.toString()});
            } else {
                chrome.browserAction.setBadgeText({text: ""});
            }
            if (window.unread < data.messages_unread && window.unread !== null) {
                notifyClient(escapeHTML(chrome.i18n.getMessage("new_sms")));
            }
            window.unread = data.messages_unread;
        }
    } catch (e) {
    }
    Utils.setTimeout(getInfo, 10000);
}

Utils.setTimeout(getInfo, 1000);

chrome.browserAction.setBadgeBackgroundColor({color: "#009688"});
