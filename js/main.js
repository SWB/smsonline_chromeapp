DEFAULT_ENCODE = function(a) { return a; };
DEFAULT_DECODE = function(a) { return a; };

var form_connexion,
    logout_btn,
    mark_as_read_btn;

function localizeHtmlPage() {
    var objects = document.getElementsByTagName('html');
    for (var j = 0; j < objects.length; j++) {
        var obj = objects[j];
        var valStrH = obj.innerHTML.toString();
        var valNewH = valStrH.replace(/__MSG_(\w+)__/g, locale);
        if(valNewH != valStrH) {
            obj.innerHTML = valNewH;
        }
    }
}

function connexion(event) {
    var email = document.getElementById("email"),
        password = document.getElementById("password");
    event.preventDefault();
    if (email.value != '' && password.value != '') {
        document.getElementById("block_form").style.display = 'none';
        document.getElementById("block_wait").style.display = 'block';

        Storage.put('email', email.value);
        var opt = {
            'url': window.api_uri + '/Users/GetToken',
            'data': {
                'email': email.value,
                'password': password.value,
                'type': "chrome_app"
            },
            'callback': connexionRes,
            'checkErrors': false,
            'decode': false
        };
        Ajax.get(opt);
    } else {
        displayFormError("empty_field");
    }
}

function connexionRes(JSONdata) {
    try {
        var data = JSON.parse(JSONdata);
        if (data.error == 0) {
            Storage.put('key', data.key);
            Storage.put('token', data.token);
            Storage.put('user', data.user);
            getInfo();
        } else if (data.error == 6) {
            displayFormError("bad_credential");
        } else {
            displayFormError("error");
        }
    } catch (e) {
        displayFormError("error");
    }
}

function displayFormError(err_str) {
    document.getElementById("block_form").style.display = 'block';
    document.getElementById("block_wait").style.display = 'none';
    document.getElementById("block_account").style.display = 'none';
    document.getElementById("error").innerHTML = escapeHTML(chrome.i18n.getMessage(err_str));
}

function getInfo() {
    var opt = {
        'url': window.api_uri + '/Users/GetInfos',
        'data': {
            'user': Storage.get('user', ''),
            'token': Storage.get('token', ''),
            'key': Storage.get('key', '')
        },
        'callback': getInfoRes,
        'checkErrors': false,
        'decode': false
    };
    Ajax.get(opt);
}

function getInfoRes(JSONdata) {
    try {
        var data = JSON.parse(JSONdata);
        if (data.error == 0) {
            document.getElementById("contact_number").innerHTML = escapeHTML(data.contacts);
            document.getElementById("message_number").innerHTML = escapeHTML(data.messages);
            document.getElementById("unread_message_number").innerHTML = escapeHTML(data.messages_unread);

            document.getElementById("block_form").style.display = 'none';
            document.getElementById("block_wait").style.display = 'none';
            document.getElementById("block_account").style.display = 'block';
            componentHandler.upgradeDom();
        } else {
            displayFormError("");
        }
    } catch (e) {
        displayFormError("");
    }
}

function markAsRead() {
    var opt = {
        'url': window.api_uri + '/Messages/markAllAsRead',
        'data': {
            'user': Storage.get('user', ''),
            'token': Storage.get('token', '')
        },
        'callback': markAsReadRes,
        'checkErrors': false,
        'decode': false
    };
    console.log(opt);
    Ajax.post(opt);
}
function markAsReadRes(data) {
}

function logout() {
    document.getElementById("block_form").style.display = 'block';
    document.getElementById("block_wait").style.display = 'none';
    document.getElementById("block_account").style.display = 'none';

    Storage.put('key', "");
    Storage.put('token', "");
    Storage.put('user', "");
}

document.onreadystatechange = function () {
    localizeHtmlPage();
    getInfo();
    document.getElementById("block_form").style.display = 'none';
    document.getElementById("block_wait").style.display = 'block';
    document.getElementById("block_account").style.display = 'none';

    document.getElementById("email").value = Storage.get('email', '');
    document.getElementById("sms_link").href = window.sms_uri;
    document.getElementById("version").innerHTML = escapeHTML(chrome.runtime.getManifest().version);

    if (document.readyState == "complete") {
        form_connexion = document.getElementById("form_connexion");
        form_connexion.onsubmit = connexion;

        logout_btn = document.getElementById("logout");
        logout_btn.onclick = logout;

        mark_as_read_btn = document.getElementById("mark_as_read_btn");
        mark_as_read_btn.onclick = markAsRead;
    }
};
